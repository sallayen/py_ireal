# -*- encoding: utf-8 -*-
# pip install flask

import flask
from flask import request
from PIL import Image
import face_recogniser.face_recogniser as fr
import io
import numpy as np
import time
import re
import os
import six.moves.urllib as urllib
import sys
import tarfile
import tensorflow as tf
import zipfile

from collections import defaultdict
from io import StringIO
from matplotlib import pyplot as plt
from PIL import Image

# This is needed since the notebook is stored in the object_detection folder.
sys.path.append("..")
#sys.path.insert(0, '/home/sallayen/py_iReal/webserver/utils')
from object_detection.utils import ops as utils_ops
#import ops as utils_ops

#if tf.__version__ < '1.4.0':
#  raise ImportError('Please upgrade your tensorflow installation to v1.4.* or later!')

# from utils import label_map_util
from utils import label_map_util

app = flask.Flask(__name__) # Crée un server web
                            # qui va gérer les URL qui lui sont envoyées
# Configure the flask server
app.config['JSON_SORT_KEYS'] = False

# Load faces models to recognise (this function can be in another file)
def load_model_faces(filename):
    with open(filename) as f:
        content = f.readlines()
    # you may also want to remove whitespace characters like `\n` at the end of each line
    content = [x.strip() for x in content]
    l = []
    for i in range(len(content)):
        if not(i % 2):
            name = content[i];
            val_str = content[i+1]
            val = [float(i) for i in val_str.split(',')]
            l.append((name, val))
    faces_model = dict(l)

    return faces_model

def tf_init_graph():
    #tensorflow object detection part
    # What model to download.
    #MODEL_NAME = 'ssd_mobilenet_v1_coco_2017_11_17'
    
    #########################
    #MODEL_NAME = 'faster_rcnn_inception_v2_coco_2018_01_28'
    MODEL_NAME = 'train10_graph'
    #MODEL_NAME = 'cisco_phone_graph'
    #MODEL_NAME = 'faster_rcnn_inception_resnet_v2_atrous_oid_2018_01_28'
    #########################
    MODEL_FILE = MODEL_NAME + '.tar.gz'
    #DOWNLOAD_BASE = 'http://download.tensorflow.org/models/object_detection/'
    # Path to frozen detection graph. This is the actual model that is used for the object detection.
    PATH_TO_CKPT = 'webserver/' + MODEL_NAME + '/frozen_inference_graph.pb'  
    detection_graph = tf.Graph()    
    with detection_graph.as_default():
      od_graph_def = tf.GraphDef()
      with tf.gfile.GFile(PATH_TO_CKPT, 'rb') as fid:
        serialized_graph = fid.read()
        od_graph_def.ParseFromString(serialized_graph)
        tf.import_graph_def(od_graph_def, name='')
    return detection_graph
    

def tf_init_index():
    # List of the strings that is used to add correct label for each box.
    #PATH_TO_LABELS = os.path.join('data', 'mscoco_label_map.pbtxt')
    #################
    PATH_TO_LABELS = os.path.join('webserver/data', 'labelmap.pbtxt')
    NUM_CLASSES = 2
    #################
    #################
    #PATH_TO_LABELS = os.path.join('webserver/data', 'mscoco_label_map.pbtxt')
    #NUM_CLASSES = 90
    #################
    #PATH_TO_LABELS = os.path.join('webserver/data', 'object-detection.pbtxt')
    #NUM_CLASSES = 1
    #################
    #PATH_TO_LABELS = os.path.join('webserver/data', 'modified_mscoco_label_map.pbtxt')
    #NUM_CLASSES = 91
    #################
    label_map = label_map_util.load_labelmap(PATH_TO_LABELS)
    categories = label_map_util.convert_label_map_to_categories(label_map, max_num_classes=NUM_CLASSES, use_display_name=True)
    category_index = label_map_util.create_category_index(categories)
    return category_index

def load_image_into_numpy_array(image):
  (im_width, im_height) = image.size
  return np.array(image.getdata()).reshape(
      (im_height, im_width, 3)).astype(np.uint8)

def run_inference_for_single_image(image, graph, sess):
      # Get handles to input and output tensors
      ops = graph.get_operations()
      all_tensor_names = {output.name for op in ops for output in op.outputs}
      tensor_dict = {}
      for key in [
          'num_detections', 'detection_boxes', 'detection_scores',
          'detection_classes', 'detection_masks'
      ]:
        tensor_name = key + ':0'        
        if tensor_name in all_tensor_names:
          tensor_dict[key] = graph.get_tensor_by_name(
            tensor_name)
      if 'detection_masks' in tensor_dict:
        # The following processing is only for single image
        detection_boxes = tf.squeeze(tensor_dict['detection_boxes'], [0])
        detection_masks = tf.squeeze(tensor_dict['detection_masks'], [0])
        # Reframe is required to translate mask from box coordinates to image coordinates and fit the image size.
        real_num_detection = tf.cast(tensor_dict['num_detections'][0], tf.int32)
        detection_boxes = tf.slice(detection_boxes, [0, 0], [real_num_detection, -1])
        detection_masks = tf.slice(detection_masks, [0, 0, 0], [real_num_detection, -1, -1])
        detection_masks_reframed = utils_ops.reframe_box_masks_to_image_masks(
            detection_masks, detection_boxes, image.shape[0], image.shape[1])
        detection_masks_reframed = tf.cast(
            tf.greater(detection_masks_reframed, 0.5), tf.uint8)
        # Follow the convention by adding back the batch dimension
        tensor_dict['detection_masks'] = tf.expand_dims(
            detection_masks_reframed, 0)
      image_tensor = graph.get_tensor_by_name('image_tensor:0')        
      # Run inference
      output_dict = sess.run(tensor_dict,
                             feed_dict={image_tensor: np.expand_dims(image, 0)})
      # all outputs are float32 numpy arrays, so convert types as appropriate
      output_dict['num_detections'] = int(output_dict['num_detections'][0])
      output_dict['detection_classes'] = output_dict[
          'detection_classes'][0].astype(np.uint8)
      output_dict['detection_boxes'] = output_dict['detection_boxes'][0]
      output_dict['detection_scores'] = output_dict['detection_scores'][0]
      if 'detection_masks' in output_dict:
        output_dict['detection_masks'] = output_dict['detection_masks'][0]
      return output_dict

# Load the features to compare and recognise faces
filename_faces_to_recognise = "/home/sallayen/photosExemple/faceEncodings.txt"
faces_to_compare = load_model_faces(filename_faces_to_recognise)
faces_to_compare_np = np.array(list(faces_to_compare.values()))
print(faces_to_compare.keys())

config = tf.ConfigProto()
#limit of 50% the total memory allowed for tensorflow 
config.gpu_options.per_process_gpu_memory_fraction = 0.7
detection_graph = tf_init_graph()
category_index = tf_init_index()
sess = tf.Session(config=config, graph=detection_graph) 
@app.route("/api/cv/object_recognition/", methods=['POST'])
def api_object_recognition():
    try:
        for f in request.files:
            img = request.files[f]
            start = time.time()             
            object_list=list()
            pimg = Image.open(img.stream)
            width, height = pimg.size 
            npimg = np.array(pimg)       
            output_dict = run_inference_for_single_image(npimg, detection_graph,sess)
            for idx, x in enumerate(output_dict['detection_scores']):
                if x > 0.5:
                    ymin = int(output_dict['detection_boxes'][idx][0]*height)
                    xmin = int(output_dict['detection_boxes'][idx][1]*width)
                    ymax = int(output_dict['detection_boxes'][idx][2]*height)
                    xmax = int(output_dict['detection_boxes'][idx][3]*width)
                    bb_dic  = {"left": xmin,
                               "top": ymin,
                               "width": xmax-xmin,
                               "height": ymax-ymin}
                    object_list.append({"name": category_index[output_dict['detection_classes'][idx]]['name'],
                                        "score": float(x),
                                        "box": bb_dic})

            response_list = [("objects", object_list),
                             ("metadata",
                              {"width": width,
                               "height": height,
                               "format": img.content_type})]
                             
            final_dict = dict(response_list)
            print(final_dict)
            end = time.time()
            print("Processing time: {} ".format(end - start))
            return flask.jsonify(final_dict)

    except Exception as err:
        print("ko:", err)

    return "ok"

# enregistrer une route qui renvoit du texte brut
# http://127.0.0.1:9099/hello
@app.route("/cv")
def hello():
    return "Computer vision server ...!"



# renvoyer du texte généré à partir d'une template
# http://127.0.0.1:9099/hello.html
@app.route('/bonjour.html')
def bonjour_html():
    mots = ["bonjour", "à", "toi,", "visiteur."]
    return flask.render_template('hello.html'
                                 , titre="Bienvenue !"
                                 , mots=mots)


# renvoyer du json, utiliser un route variable
# http://127.0.0.1:9099/api/product/A
PRODUCTS = {
   "A": {'title': 'Mouse', 'price': 50.},
   "B": {'title': 'Keyboard', 'price': 45.}
}         

# respecte les conventions d'une api REST
@app.route("/api/product/<id>")
def product(id=None):
    # Si on saisi http://localhost:8081/api/product/A => id='A'
    product_info = PRODUCTS[id]
    return flask.jsonify(identifiant=id
                         , titre= product_info['title']
                         , prix= product_info['price'])


# créer une route spécifique à une méthode
# ajouter des données avec put
# renvoyer un code de succès autre que 200
# POST http://127.0.0.1:9099/api/product/C  (utiliser addon firefox REST easy)
# puis pour consulter le résultat: http://127.0.0.1:5000/api/product/C

@app.route("/api/product/<id>",
           methods=["PUT", "POST"])
def edit_product(id=None):

    data = "Received data:\n"
    # data += request.form['test']
    data += "\n" + request.data.decode('utf8')
    data += "param1:" + request.form['param1']
    data += ", "
    data += "param2:" + request.form['param2']

    return data


@app.route("/api/cv/face_detection/", methods=['POST'])
def api_face_detection():
    try:
        for f in request.files:
            img = request.files[f]
            print(f, type(img))
            print(dir(img))
            print(type(img.stream))
            print(dir(img.stream))

            pimg = Image.open(img.stream)
            npimg = np.array(pimg)
            bounding_boxes = fr.face_locations(npimg)

            faces_list = list()
            for bb in bounding_boxes:
                print(bb)
                bb_dic = {"left": bb[3],
                          "top": bb[0],
                          "width": bb[1] - bb[3],
                          "height": bb[2] - bb[0]}
                print(bb_dic)
                faceId = "unknown"
                faces_list.append({"faceId": faceId,
                                   "faceRectangle": bb_dic})

            response_list = [("faces", faces_list),
                             ("metadata",
                              {"width": pimg.width,
                               "height": pimg.height,
                               "format": img.content_type})]

            response_dict = dict(response_list)

            print(response_dict)
            return flask.jsonify(response_dict)

    except Exception as err:
        print("ko:", err)

    return "ok"


@app.route("/api/cv/face_detection_cnn/", methods=['POST'])
def api_face_detection_cnn():
    try:
        for f in request.files:
            img = request.files[f]

            pimg = Image.open(img.stream)
            npimg = np.array(pimg)
            bounding_boxes = fr.face_locations(npimg, 1, 'cnn')

            faces_list = list()
            for bb in bounding_boxes:
                bb_dic = {"left": bb[3],
                          "top": bb[0],
                          "width": bb[1]-bb[3],
                          "height": bb[2]-bb[0]}
                faceId = "unknown"
                faces_list.append({"faceId": faceId,
                                  "faceRectangle": bb_dic})

            response_list = [("faces", faces_list),
                             ("metadata",
                              {"width": pimg.width,
                               "height": pimg.height,
                               "format": img.content_type})]

            response_dict = dict(response_list)

            print(faces_list)
            print('/n')
            return flask.jsonify(response_dict)

    except Exception as err:
        print("ko:", err)

    return "ok"


@app.route("/api/cv/face_recognition/", methods=['POST'])
def api_face_recognition():
    try:
        for f in request.files:
            img = request.files[f]
            start = time.time()
            pimg = Image.open(img.stream)
            npimg = np.array(pimg)
            bounding_boxes = fr.face_locations(npimg, 1, 'cnn')
            face_encodings = fr.face_encodings(npimg, bounding_boxes)
            face_names = []

            for face_encoding in face_encodings:
                # See if the face is a match for the known face(s)
                distances = np.linalg.norm(faces_to_compare_np - face_encoding, axis=1)
                tolerance = 0.6
                match = list(distances <= tolerance)
                name = "Unknown__"
                true_index = [i for i, x in enumerate(match) if x]

                if any(match):
                    name = list(faces_to_compare.keys())[true_index[0]]
                print(name)
                face_names.append(name)


            faces_list = list()
            for idx, bb in enumerate(bounding_boxes):
                bb_dic = {"left": bb[3],
                          "top": bb[0],
                          "width": bb[1]-bb[3],
                          "height": bb[2]-bb[0]}
                faceId1 = face_names[idx][:-2]
                faceId2 = re.sub(r"(\w)([A-Z])", r"\1 \2", faceId1)
                faceId = faceId2.title()
                print(faceId)
                faces_list.append({"faceId": faceId,
                                  "faceRectangle": bb_dic})

            response_list = [("faces", faces_list),
                             ("metadata",
                              {"width": pimg.width,
                               "height": pimg.height,
                               "format": img.content_type})]

            response_dict = dict(response_list)

            print(faces_list)
            end = time.time()
            print("Processing time: {} ".format(end - start))
            return flask.jsonify(response_dict)

    except Exception as err:
        print("ko:", err)

    return "ok"

@app.route("/api/cv/face_object_recognition/", methods=['POST'])
def api_face_object_recognition():
    try:
        for f in request.files:
            img = request.files[f]
            start = time.time()
            pimg = Image.open(img.stream)
            width, height = pimg.size 
            npimg = np.array(pimg)
            bounding_boxes = fr.face_locations(npimg, 1, 'cnn')
            face_encodings = fr.face_encodings(npimg, bounding_boxes)
            face_names = []

            for face_encoding in face_encodings:
                distances = np.linalg.norm(faces_to_compare_np - face_encoding, axis=1)
                tolerance = 0.6
                match = list(distances <= tolerance)
                name = "Unknown__"
                true_index = [i for i, x in enumerate(match) if x]
                if any(match):
                    name = list(faces_to_compare.keys())[true_index[0]]
                face_names.append(name)
                print(name)
            faces_list = list()
            for idx, bb in enumerate(bounding_boxes):
                bb_dic = {"left": bb[3],
                          "top": bb[0],
                          "width": bb[1]-bb[3],
                          "height": bb[2]-bb[0]}
                faceId1 = face_names[idx][:-2]
                faceId2 = re.sub(r"(\w)([A-Z])", r"\1 \2", faceId1)
                faceId = faceId2.title()
                faces_list.append({"faceId": faceId,
                                  "faceRectangle": bb_dic})
                print(faceId)
            object_list = list()
            output_dict = run_inference_for_single_image(npimg, detection_graph,sess)
            for idx, x in enumerate(output_dict['detection_scores']):
                #if x > 0.96: #0.6 originally
                if x > 0.6: #0.6 originally
                    ymin = int(output_dict['detection_boxes'][idx][0]*height)
                    xmin = int(output_dict['detection_boxes'][idx][1]*width)
                    ymax = int(output_dict['detection_boxes'][idx][2]*height)
                    xmax = int(output_dict['detection_boxes'][idx][3]*width)
                    bb_dic  = {"left": xmin,
                               "top": ymin,
                               "width": xmax-xmin,
                               "height": ymax-ymin}
                    object_list.append({"name": category_index[output_dict['detection_classes'][idx]]['name'],
                                        "score": float(x),
                                        "box": bb_dic})

            response_list = [("faces", faces_list),
                             ("objects", object_list),
                             ("metadata",
                              {"width": width,
                               "height": height,
                               "format": img.content_type})]

            response_dict = dict(response_list)                    
            print(response_dict)
            end = time.time()
            print("Processing time: {} ".format(end - start))
            return flask.jsonify(response_dict)

    except Exception as err:
        print("ko:", err)

    return "ok"


@app.route("/api/product/add", methods=["POST"])  # respecte les conventions d'une api REST
def add_product():
    id = request.form['id']
    print('PRODUITS en base:', PRODUCTS)

    print("Reçu:", request.form)
    print("ID:*%s*" % id)
    if id not in PRODUCTS:
        print("Pas trouvé")
        PRODUCTS[id] = {
           'title': request.form['title']
           , 'price': float(request.form['price'])
        }
        print("Updated")
        print(PRODUCTS[id])
        return "", 204  # 204 est le code d'erreur HTTP pour mise à jour
    else:
        return ("Aborting")
        # abort(400)

# rediriger vers une autre url
from flask import redirect, url_for
@app.route('/')
def index():
    return redirect(url_for('hello_html'))


# envoyer un code d'erreur
from flask import abort
@app.route('/broken')
def broken_url():
    abort(401)


# exercices
# renvoyer une erreur 404 si on essaye d'afficher un produit qui n'existe pas
# ajouter une route permettant de créer un produit existant (méthode POST)
# renvoyer une 400 si on essaye de créer un produit avec un id qui existe déjà
# faire un affichage html d'une fiche produit (s'inspirer de la template hello.html)


if __name__ == "__main__":
    # app.config.update(MAX_CONTENT_LENGTH=100*10**6)
    app.run(port=9099
            ,host='0.0.0.0')
