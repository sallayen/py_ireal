import os
import re
import numpy as np
import face_recognition as fr

#def write_numpy_array(filepath, array):
#    np.save(filename, array)


def writeNamesEncodingFile(filepath, lNamesEnc):
    with open(filepath, 'w') as f:
        for name in lNamesEnc:
            f.write(name[0])
            f.write('\n')
            #for line in name[1]:
            #   f.write('%.10f' % line)
            #    f.write('\n')

            name[1].tofile(f, ",", "%10s")
            f.write('\n')


def image_files_in_folder(folder):
    return [os.path.join(folder, f) for f in os.listdir(folder) if re.match(r'.*\.(jpg|jpeg|png)', f, flags=re.I)]


def encode_folder(folderpath):

    names = []
    face_encodings = []
    list_names_encodings = []

    for filename in image_files_in_folder(folderpath):

        basename = os.path.splitext(os.path.basename(filename))[0]
        print("Encoding image {}".format(filename))
        image = fr.load_image_file(filename)
        face_encoding = fr.face_encodings(image)
        print(type(face_encoding[0]))

        if len(face_encoding) > 1:
            print("More than one face found in {}. Only considering the first face.".format(filename))

        if len(face_encoding) == 0:
            print("WARNING: No faces found in {}. Ignoring file.".format(filename))

        else:
            list_names_encodings.append((basename, face_encoding[0]))

    dict_names_encodings = dict(list_names_encodings)
    writeNamesEncodingFile('/home/sallayen/photosExemple/faceEncodings.txt', list_names_encodings)

    return dict_names_encodings



folderpath = '/home/sallayen/photosExemple/'
encode_folder(folderpath)
