import unittest
import face_detector
from PIL import Image
import time

class TestReader(unittest.TestCase):
    def test_face_detection(self):
        pimg = Image.open('img_test.jpg').convert('RGB')
        start_time = time.time()
        bounding_boxes = face_detector.face_detection(pimg)
        print("--- %s seconds ---" % (time.time() - start_time))
        self.assertEqual(bounding_boxes, [[583, 282, 712, 411]])

    def test_face_detection_cnn(self):
        pimg = Image.open('img_test.jpg').convert('RGB')
        start_time = time.time()
        bounding_boxes = face_detector.face_detection_cnn(pimg)
        print("--- %s seconds ---" % (time.time() - start_time))
        self.assertEqual(bounding_boxes, [[603, 282, 701, 380]])


if __name__  == '__main__':
    unittest.main()