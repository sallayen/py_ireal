import numpy as np

def load_model_faces(filename):
    with open(filename) as f:
        content = f.readlines()
    # you may also want to remove whitespace characters like `\n` at the end of each line
    content = [x.strip() for x in content]
    print(len(content))
    print(content)
    print(content[0])
    print(content[2])
    print(content[4])
    print(content[6])
    print(content[8])
    print(content[10])
    l = []
    for i in range(len(content)):
        if not(i % 2):
            name = content[i];
            val_str = content[i+1]
            val = [float(i) for i in val_str.split(',')]
            l.append((name, val))
    faces_model = dict(l)

    return faces_model


# Load the features to compare and recognise faces
filename_faces_to_recognise = "/Users/mdiazmel/tmp/faceEncodings.txt"
faces_to_compare = load_model_faces(filename_faces_to_recognise)
faces_to_compare_np = np.array(list(faces_to_compare.values()))

print(faces_to_compare.keys)
print(faces_to_compare['mauricioDiaz_1'][0:10])
print(type(faces_to_compare_np))
print(faces_to_compare_np.shape)