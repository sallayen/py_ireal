from setuptools import setup

setup(
    name = 'face_recogniser',
    version = '0.0.1',
    description = 'Functions for face recognition using DLIB python wrappers',
    url = '',
    packages = ['face_recogniser',],
    entry_points = {
        'console_scripts': [
            'ply = face_recogniser.main:main',
        ],
    },
    license = 'BSD',
    author = 'Mauricio Diaz',
    author_email = 'mauricio.diaz-melo@inria.fr',
)
