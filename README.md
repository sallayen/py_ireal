# iReal : Implementing interactive scene understanding for a mixed reality device (Hololens project)
# Back-end module (Python version)

This repository host the back-end module for the project **iReal**.
For a general description of the project, please refer to the 
[iReal repository](https://gitlab.inria.fr/sallayen/ireal)

  

## Descripton of the Back-end module

The back-end module contains three major components:

 * A **webserver** written in Python using the Flask library. This code should be installed on the server that will perform the computer vision processings.

 * A **webrequest** example code that consists of a webpage on htlm code intended to demostrate an (eventually) test the webserver.

 * A **computer vision (cv)** folder containing the code that implements the computer vision (e.g, face detection, face recognition, object detection) algorithms.

## The webserver component

The code is developed in Pyhton. It uses the Flask library to implement the 
webserver.

### Installing the webserver
To install the webserver we proposes to create an environment Conda within all the dependencies.

- Install [Anaconda](https://www.anaconda.com/download) or [miniconda]https://conda.io/miniconda.html
- Create a conda environnement 

```
conda create --name cv_server python=3.5 numpy matplotlib flask pillow scipy 

```
- Activate the environment

```
source activate cv_server

```

- Setup the [Dlib](http://dlib.net/) library (only the python wrappers are necessaries)

```
git clone https://github.com/davisking/dlib.git
git checkout v19.9
# If you have a gpu device available, use --yes DLIB_USE_CUDA
python setup.py install --yes USE_AVX_INSTRUCTIONS --no DLIB_USE_CUDA

```


- Once you have cloned the **py_iReal** project, go to the folder *cv/face_recogniser* and install the face recognition tool in order to call the **Dlib** functions: 

```
cd py_iReal/cv/face_recogniser/
pip install -e .

```


- For the object recognition at least Tensorflow 1.10 must be installed with CUDA-9.0


- Launch the server 

```
python webserver/server.py

```

Next time, when you will to launch the server, the only thing that you must do is to activate the conda environment (*source activate cv_server*) and launch the *server.py* script.


### Modify the webserver

Inside the server.py there is a face detection and object detection. The object detection can be linked to global ms coco recognition or a custom model. To change it you have to modify

MODEL_NAME = 'faster_rcnn_inception_v2_coco_2018_01_28' or your custom model
PATH_TO_LABELS = os.path.join('webserver/data', 'mscoco_label_map.pbtxt') or your custom labelmap
NUM_CLASSES = 90 or the number of objects in your model